# Aira-Test

This is a simple project where you can log in and get a token by using JWT. 
Once logged in you can make quotes and get the result using ajax. 

The frontend is made with blade templates and jquery to show the basic functionalities. The ideal would be to use React or VueJs because they have an independent control of routes and data in communication with the Rest api.
The token is stored in the localstorage as an example, but ideally it is defined in the cookies using HttpOnly.

# Starting

## Commands

```
git clone https://gitlab.com/santiagopegels/airo-test.git
```

```
cd airo-test
```

```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

```
cp .env.real .env
```

```
./vendor/bin/sail up -d
```

```
./vendor/bin/sail php artisan migrate:fresh --seed
```

Ready to use: http://localhost

## Default User
```
email: test@example.com
password: test123
```

## Postman
 You can use the postman collection located in the root of the project for communication with the api rest.
    