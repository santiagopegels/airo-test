<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'symbol' => 'EUR',
                'description' => 'Euros',
                'current_exchange_value' => 1
            ],
            [
                'symbol' => 'GBP',
                'description' => 'Pound sterling',
                'current_exchange_value' => 0.85
            ],
            [
                'symbol' => 'USD',
                'description' => 'Pound sterling',
                'current_exchange_value' => 1.07
            ]
        ];

        foreach ($currencies as $currency) {
            Currency::create([
                'symbol' => $currency['symbol'],
                'description' => $currency['description'],
                'current_exchange_value' => $currency['current_exchange_value']
            ]);
        }
    }
}
