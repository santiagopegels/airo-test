<?php

namespace Database\Seeders;

use App\Models\AgeLoad;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AgeLoadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ageLoads = [
            [
                'start' => 18,
                'end' => 30,
                'load_value' => 0.6
            ],
            [
                'start' => 31,
                'end' => 40,
                'load_value' => 0.7
            ],
            [
                'start' => 41,
                'end' => 50,
                'load_value' => 0.8
            ],
            [
                'start' => 51,
                'end' => 60,
                'load_value' => 0.9
            ],
            [
                'start' => 61,
                'end' => 70,
                'load_value' => 1
            ]
        ];

        foreach ($ageLoads as $ageLoad) {
            AgeLoad::create([
                'start_age' => $ageLoad['start'],
                'end_age' => $ageLoad['end'],
                'load_value' => $ageLoad['load_value']
            ]);
        }
    }
}
