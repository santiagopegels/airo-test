<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'currencies';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'symbol',
        'description',
        'current_exchange_value'
    ];

    public static function currentExchangeValue(string $currency)
    {
        return self::query()->where('symbol', $currency)->value('current_exchange_value');
    }

    public static function getBySymbol(string $currency)
    {
        return self::query()->where('symbol', $currency)->first();
    }
}
