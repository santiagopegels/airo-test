<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgeLoad extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'age_loads';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'start_age',
        'end_age',
        'load_value'
    ];

    public static function calculateLoadSum(array $ages)
    {
        $total = 0;
        foreach ($ages as $age){
           $total += self::query()->where('start_age', '<=', $age)
                        ->where('end_age', '>=', $age)
                        ->value('load_value');
        }

        return $total;
    }

    public static function getAgesArrayFromString(string $ages)
    {
        return explode(',', $ages);
    }
}
