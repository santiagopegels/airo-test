<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quotation extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $table = 'quotations';

    protected $dates = ['deleted_at'];

    protected $casts = [
        'total' => 'decimal:2'
    ];

    public $fillable = [
        'ages',
        'user_id',
        'currency_id',
        'start_date',
        'end_date',
        'total'
    ];

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function toArray()
    {
        return [
            'total' => (float) $this->total,
            'currency_id' => $this->currency->symbol,
            'quotation_id' => $this->id
        ];
    }
}
