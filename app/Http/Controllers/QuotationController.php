<?php

namespace App\Http\Controllers;

use App\Services\QuotationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuotationController extends Controller
{
    private QuotationService $service;

    public function __construct(QuotationService $service)
    {
        $this->middleware('auth:api');
        $this->service = $service;
    }

    public function calculateQuotation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'currency_id' => 'required|exists:currencies,symbol',
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d',
            'ages' => 'required|regex:/^[\d\s,]*$/'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        $currencyId = $request->get('currency_id');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');
        $ages = $request->get('ages');

        $quotation = $this->service->generateQuotation($currencyId, $startDate, $endDate, $ages);

        return response()->json([
            'data' => $quotation->toArray()
        ]);
    }
}
