<?php

namespace App\Services;

use App\Models\AgeLoad;
use App\Models\Currency;
use App\Models\Quotation;
use Carbon\Carbon;
use Faker\Provider\bn_BD\Utils;
use Illuminate\Support\Facades\Config;

class QuotationService
{
    public function generateQuotation(string $currentId, string $startDate, string $endDate, string $ages): Quotation
    {
        $startDate = Carbon::createFromFormat('Y-m-d', $startDate);
        $endDate = Carbon::createFromFormat('Y-m-d', $endDate);
        $diffInDaysBetweenDates = UtilsService::datesDiffInDays($startDate, $endDate);
        $agesArray = AgeLoad::getAgesArrayFromString($ages);
        $currency = Currency::getBySymbol($currentId);
        $loadSumTotal = AgeLoad::calculateLoadSum($agesArray);

        $totalQuotation = $this->calculateQuotation($loadSumTotal, $diffInDaysBetweenDates, $currency->current_exchange_value);

       $quotation = Quotation::create([
            'user_id' => auth()->user()->id,
            'currency_id' => $currency->id,
            'start_date' => $startDate,
            'end_date' => $endDate,
            'total' => $totalQuotation,
            'ages' => $ages
        ]);

        $quotation->save();

        return $quotation;
    }

    private function calculateQuotation(float $loadSumTotal, int $diffInDaysBetweenDates, float $currencyExchangeValue)
    {
        return $loadSumTotal * $diffInDaysBetweenDates * Config::get('constants.FIXED_RATE') * $currencyExchangeValue;
    }


}
