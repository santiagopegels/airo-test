<?php

namespace App\Services;

use Carbon\Carbon;

class UtilsService
{
    public static function datesDiffInDays(Carbon $startDate, Carbon $endDate): int
    {
        return $startDate->diffInDays($endDate);
    }
}
