@extends('layout.app')

@section('content')
    <!-- Email input -->
    <form id="login-form">
        <span class="text-danger" id="loginError"></span>

        <div class="form-outline mb-4">
            <input type="email" id="email" class="form-control" value="test@example.com"/>
            <label class="form-label" for="email">Email address</label>
        </div>

        <!-- Password input -->
        <div class="form-outline mb-4">
            <input type="password" id="pass" class="form-control" value="test123"/>
            <label class="form-label" for="pass">Password</label>
        </div>

        <!-- Submit button -->
        <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
    </form>
@endsection

@push('scripts')
    <script>

        $('#login-form').submit((e) => {
            e.preventDefault();
            let email = $('#email').val();
            let pass = $('#pass').val();

            $.ajax({
                url: "api/login",
                type: "POST",
                data: {
                    email: email,
                    password: pass,
                }
            }).then(res => {
                localStorage.setItem('token', res.authorisation.token)
                document.location.href="/";

            }).catch(error => {
                console.log(error.responseJSON)
                if (error.responseJSON.error) {
                    $('#loginError').text(Object.values(error.responseJSON.error)[0]);
                }
            });
        })
    </script>
@endpush
