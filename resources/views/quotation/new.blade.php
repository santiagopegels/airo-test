@extends('layout.app')


@section('content')

    <form id="quotation-form" novalidate class="needs-validation row g-3">
        <div class="col-12">
            <label for="ages" class="form-label">Ages</label>
            <input type="text" class="form-control" id="ages" placeholder="25,35,48">
            <span class="text-danger" id="agesError"></span>
        </div>
        <div class="col-12">
            <label for="currency_id" class="form-label">Currency</label>
            <select class="custom-select" id="currency_id">
                <option selected>Choose...</option>
                <option value="EUR">EUR</option>
                <option value="GBP">GBP</option>
                <option value="USD">USD</option>
            </select>
            <span class="text-danger" id="currencyError"></span>
        </div>
        <div class="col-md-6">
            <label for="start_date">From</label>
            <input id="start_date" name="start_date" class="datepicker form-control" type="text" />
            <span class="text-danger" id="startDateError"></span>
        </div>
        <div class="col-md-6">
            <label for="end_date">To</label>
            <input id="end_date" name="end_date" class="datepicker form-control" type="text" />
            <span class="text-danger" id="endDateError"></span>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary mt-2">Submit</button>
        </div>
    </form>

    <div class="mt-4">
        <div class="col-12">
            <label for="total" class="form-label">Total</label>
            <input type="text" class="form-control" id="total" disabled>
        </div>
        <div class="col-12">
            <label for="currency_result" class="form-label">Currency Response</label>
            <input type="text" class="form-control" id="currency_result" disabled>
        </div>
        <div class="col-12">
            <label for="quotation_id" class="form-label">Quotation ID</label>
            <input type="text" class="form-control" id="quotation_id" disabled>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        } );

        $('#quotation-form').submit((e)=> {
            e.preventDefault();

            let ages = $('#ages').val();
            let currency_id = $('#currency_id').val();
            let start_date = $('#start_date').val();
            let end_date = $('#end_date').val();

            $.ajax({
                url: "api/quotation",
                type:"POST",
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                },
                data:{
                    ages:ages,
                    currency_id:currency_id,
                    start_date:start_date,
                    end_date:end_date,
                }
            }).then(res => {
                console.log(res)
                $('#currency_result').val(res.data.currency_id);
                $('#total').val(res.data.total);
                $('#quotation_id').val(res.data.quotation_id);
            }).catch(error => {
                console.log(error.responseJSON.error)
                $('#agesError').text(error.responseJSON.error.ages ? error.responseJSON.error.ages : '');
                $('#currencyError').text(error.responseJSON.error.currency_id ? error.responseJSON.error.currency_id : '');
                $('#startDateError').text(error.responseJSON.error.start_date ? error.responseJSON.error.start_date : '');
                $('#endDateError').text(error.responseJSON.error.end_date ? error.responseJSON.error.end_date : '');
            });
        })
    </script>
@endpush


